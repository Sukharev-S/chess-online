let divSquare = '<div id="s$coord" class="square $color"></div>';
let divFigure = '<div id="f$coord" class="figure">$figure</div>';
let isDragging = false;
let isFlipped = false;
let map;

$(function () {
  start();
  setInterval('showFiguresPHP()', 1000);
  $('.buttonNew').click(newFiguresPHP);
  $('.buttonFlip').click(flipBoard);
});

function start () {
  map = new Array(64);
  addSquares();
  showFiguresPHP();
}

function setDraggable() {
  $('.figure').draggable({
    start:  function (event, ui) {
        isDragging = true;
    }
  });
}

function setDroppable() {
  $('.square').droppable({
    drop: function (event, ui) {
        let fromCoord = ui.draggable[0].id.substring(1);
        let toCoord = event.target.id.substring(1);
        moveFigure(fromCoord, toCoord);
        moveFigurePHP(fromCoord, toCoord);
        isDragging = false;
    }
  });
}

function moveFigure(fromCoord, toCoord) {
  figure = map[fromCoord];
  showFigureAt(fromCoord, '1');
  showFigureAt(toCoord, figure);
}

function addSquares() {
  $('.board').html('');
  for(let coord = 0; coord < 64; coord++) {
    $('.board').append(divSquare
        .replace('$coord', isFlipped ? 63 - coord : coord)
        .replace('$color', isBlackSquareAt(coord) ? 'black' : 'white'));
  }
  setDroppable();
}

function showFigures(figures) {
  for (let coord = 0; coord < 64; coord++) {
    showFigureAt(coord, figures.charAt(coord))
  }
}

function showFigureAt(coord, figure) {
  if (map[coord] == figure) return;
  map[coord] = figure;
  $('#s' + coord).html(divFigure
      .replace('$coord', coord)
      .replace('$figure', getChessSymbole(figure)));
  setDraggable();
}

function getChessSymbole(figure) {
  switch (figure) {
    case 'K'  : return '&#9812;';
    case 'Q'  : return '&#9813;';
    case 'R'  : return '&#9814;';
    case 'B'  : return '&#9815;';
    case 'N'  : return '&#9816;';
    case 'P'  : return '&#9817;';
    case 'k'  : return '&#9818;';
    case 'q'  : return '&#9819;';
    case 'r'  : return '&#9820;';
    case 'b'  : return '&#9821;';
    case 'n'  : return '&#9822;';
    case 'p'  : return '&#9823;';
    default   : return '';
  }
}

function isBlackSquareAt(coord) {
  return (coord % 8 + Math.floor(coord/8)) % 2;
}

function flipBoard() {
  isFlipped = !isFlipped;
  start();
}

function moveFigurePHP(fromCoord, toCoord) {
  $.get('chess.php?moveFigure' + '&fromCoord=' + fromCoord + '&toCoord=' + toCoord, showFigures);
}

function showFiguresPHP() {
  console.log('showFiguresPHP')
  if (isDragging) return;
  $.get('chess.php?getFigures', showFigures);
}

function newFiguresPHP() {
  $.get('chess.php?newFigures', showFigures);
}